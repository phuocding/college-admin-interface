function convertDateTime(value){
    var date = new Date(Date.parse(value));
    return (date.getDate()) + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() ;
}

function generateTextEditor(){
    var editor = new Simditor({
        textarea: $('#editor'),
        toolbar:
            [
                'title',
                'bold',
                'italic',
                'underline',
                'strikethrough',
                'fontScale',
                'color',
                'ol',
                'ul' ,
                'blockquote',
                'code'  ,
                'table',
                'link',
                'image',
                'hr'    ,
                'indent',
                'outdent',
                'alignment',
            ]
    });
}
$(document).ready(function () {
    $("#userName").text("Welcome " + Cookies.get("loggedUserName"));
    var token = Cookies.get("token");
    var role = Cookies.get("loggedUserRole");
    if(token == null || role == null || role.split("#").includes("Employee") == false){
        alert(("You don't have permission to view this page"));
        Cookies.remove('token');
        Cookies.remove("loggedUserRole");
        Cookies.remove("loggedUserName");
        Cookies.remove("loggedUserId");
        window.location.href = "login.html";
    }

    $("#logout").click(function () {
        Cookies.remove('token');
        Cookies.remove("loggedUserRole");
        Cookies.remove("loggedUserName");
        window.location.href = "login.html";
    });
});