$(document).ready(function () {
    generateTextEditor();
  $.ajax({
      type: 'POST',
      accepts: 'application/json',
      contentType: 'application/json',
      url: API_DepartmentDetails,
      headers: {
          "Authorization": Cookies.get("token"),
      },
      data: JSON.stringify(window.location.href.split("=")[1]),
      success: function (result) {
          $("#editForm").find('input[name="Name"]').val(result[0].name);
          $("#editForm").find('textarea[name="Description"]').val(result[0].description);
          $(".simditor-body").eq(0).html(result[0].descriptionDetails);
          $("#editForm").find('input[name="image"]').val(result[0].imageUrl);
          $("#editForm").find('input[type="radio"]').each(function () {
              if(result[0].status == $(this).val()){
                  $(this).prop('checked', true);
              }
          });

          $($.parseHTML('<img>')).attr('src', result[0].imageUrl).appendTo(".preview_images");
          $("img").addClass("preview_image");
          $('.preview_images').removeClass("hidden");

      },
      error: function (xhr, textStatus, errorThrown) {
          alert("error");
      }
  });

  $(".btn-submit").click(function (){
      var formData = {
          "Name" : $("#editForm").find('input[name="Name"]').val(),
          "Description" : $("#editForm").find('textarea[name="Description"]').val(),
          "DescriptionDetails" : $(".simditor-body")[0].innerHTML,
          "DepartmentId" : window.location.href.split("=")[1],
          "ImageUrl" : $("#editForm").find('input[name="image"]').val(),
          "Status" : $("#editForm").find('input[name="Status"]:checked').val(),
      };
          $.ajax({
              type: 'POST',
              accepts: 'application/json',
              contentType: 'application/json',
              url: API_EditDepartment,
              headers: {
                  "Authorization": Cookies.get("token"),
                  "Role": Cookies.get("loggedUserRole")
              },
              data: JSON.stringify(formData),
              success: function (result) {
                  swal("Updated Department Successful");
              },
              error: function (xhr, textStatus, errorThrown) {
                  alert("error");
              }
          });
  });

    $('#add_images').on('propertychange input', function (e) {
        var valueChanged = false;
        if (e.type=='propertychange') {
            valueChanged = e.originalEvent.propertyName=='value';
        } else {
            valueChanged = true;
        }
        if (valueChanged) {
            $('.preview_images').html("");
            var imgUrl = $("#editForm").find('input[name="image"]').val();
            $($.parseHTML('<img>')).attr('src', imgUrl).appendTo(".preview_images");
            $("img").addClass("preview_image");
            $('.preview_images').removeClass("hidden");
        }
    });
});