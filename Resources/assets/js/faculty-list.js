$(document).ready(function () {
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_FacultiesIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Faculty Name</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>\n" +
                "                                        <tbody>";
            for (var i in result){
                content += "<tr>";
                content += "<td>" + result[i].name + "</td>";
                content += "<td><a href='faculty-edit.html?facultyId=" + result[i].facultyId + "'> Edit </a>";
                content += "<a href='faculty-detail.html?facultyId=" + result[i].facultyId + "'>" + "Details </a>";
                content += "<a class='delete-faculty " + result[i].facultyId + "'>" + "Delete </a>";
                content += "</td>";
                content += "</tr>";
            }
            content += "</tbody>\n" +
                "\n" +
                "                                    </table>";
            $("#listFaculties").html(content);
            $('#listFaculties').find('.table').DataTable();
            console.log(result)
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });
});