$(document).ready(function () {
    generateTextEditor();
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_DepartmentsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "";
            for (var i in result){
                    content += "<option value='" + result[i].departmentId + "'>" + result[i].name + "</option>"
            }
            $("#departmentList").html(content);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

  $.ajax({
      type: 'POST',
      accepts: 'application/json',
      contentType: 'application/json',
      url: API_FacultyDetails,
      headers: {
          "Authorization": Cookies.get("token"),
      },
      data: JSON.stringify(window.location.href.split("=")[1]),
      success: function (result) {
          $("#editForm").find('input[name="Name"]').val(result.faculty[0].name);
          $("#editForm").find('input[name="image"]').val(result.faculty[0].imageUrl);
          $("#editForm").find('textarea[name="Description"]').val(result.faculty[0].description);

          $( document ).ajaxStop(function() {
              $("#departmentList").val(result.faculty[0].department.departmentId);
          });

          $("#editForm").find('input[type="radio"]').each(function () {
              if(result.faculty[0].status == $(this).val()){
                  $(this).prop('checked', true);
              }
          });
          $(".simditor-body").eq(0).html(result.faculty[0].descriptionDetails);
          $($.parseHTML('<img>')).attr('src', result.faculty[0].imageUrl).appendTo(".preview_images");
          $("img").addClass("preview_image");
          $('.preview_images').removeClass("hidden");

      },
      error: function (xhr, textStatus, errorThrown) {
          alert("error");
      }
  });

  $(".btn-submit").click(function (){
      var classIds = [];
      $.each($("#editForm").find('select[name="classIds[]"]'), function () {
          $(this).val().forEach(function (item) {
              classIds.push(item.replace("class", ""));
          })
      });
      var formData = {
          "Name" : $("#editForm").find('input[name="Name"]').val(),
          "Description" : $("#editForm").find('textarea[name="Description"]').val(),
          "DescriptionDetails" : $(".simditor-body")[0].innerHTML,
          "DepartmentId" : $("#editForm").find('select[name="departmentId"]').val(),
          "Status" : $("#editForm").find('input[name="Status"]:checked').val(),
          "FacultyId": window.location.href.split("=")[1],
          "ImageUrl" : $("#editForm").find('input[name="image"]').val(),
          "SubjectIds" : $("#editForm").find('select[name="subjectIds[]"]').val(),
          "ClazzIds" : classIds,
      };
          $.ajax({
              type: 'POST',
              accepts: 'application/json',
              contentType: 'application/json',
              url: API_EditFaculty,
              headers: {
                  "Authorization": Cookies.get("token"),
                  "Role": Cookies.get("loggedUserRole")
              },
              data: JSON.stringify(formData),
              success: function (result) {
                  swal("Updated Faculty Successful");
              },
              error: function (xhr, textStatus, errorThrown) {
                  alert("error");
              }
          });
  });

    $('#add_images').on('propertychange input', function (e) {
        var valueChanged = false;
        if (e.type=='propertychange') {
            valueChanged = e.originalEvent.propertyName=='value';
        } else {
            valueChanged = true;
        }
        if (valueChanged) {
            $('.preview_images').html("");
            var imgUrl = $("#editForm").find('input[name="image"]').val();
            $($.parseHTML('<img>')).attr('src', imgUrl).appendTo(".preview_images");
            $("img").addClass("preview_image");
            $('.preview_images').removeClass("hidden");
        }
    });
});