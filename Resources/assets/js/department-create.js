$(document).ready(function () {
    generateTextEditor();
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_FacultiesIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "";
            for (var i in result){
                    if(result[i].inDepartmentStatus == 0){
                        content += "<option value='" + result[i].facultyId + "'>" + result[i].name + "</option>"
                    }
            }
            $("#facultyList").html(content);
            $('#facultyList').multiselect({
                enableFiltering: true,
                disableIfEmpty: true,
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $(".btn-submit").click(function (){
        if($("#createDepartmentForm")[0].checkValidity()) {
            var formData = {
                "Name" : $("#createDepartmentForm").find('input[name="Name"]').val(),
                "Description" : $("#createDepartmentForm").find('textarea[name="Description"]').val(),
                "DescriptionDetails" : $(".simditor-body")[0].innerHTML,
                "FacultiesIds" : $("#createDepartmentForm").find('select[name="facultiesIds[]"]').val(),
                "ImageUrl" : $("#createDepartmentForm").find('input[name="image"]').val(),
            };
            $.ajax({
                type: 'POST',
                accepts: 'application/json',
                contentType: 'application/json',
                url: API_CreateDepartment,
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                data: JSON.stringify(formData),
                success: function (result) {
                    swal("Create New Department Successful");
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (xhr.status == 409) {
                        alert("Department Existed");
                    }
                }
            });
        }
        else {
            $("#createDepartmentForm")[0].reportValidity();
        }
    });

    $('#add_images').on('propertychange input', function (e) {
        var valueChanged = false;
        if (e.type=='propertychange') {
            valueChanged = e.originalEvent.propertyName=='value';
        } else {
            valueChanged = true;
        }
        if (valueChanged) {
            $('.preview_images').html("");
            var imgUrl = $("#createDepartmentForm").find('input[name="image"]').val();
            $($.parseHTML('<img>')).attr('src', imgUrl).appendTo(".preview_images");
            $("img").addClass("preview_image");
            $('.preview_images').removeClass("hidden");
        }
    });

    $(":reset").click(function (){
        $(".preview_images").addClass('hidden');
        $(".preview_image").remove();
    });
});