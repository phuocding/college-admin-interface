$(document).ready(function () {
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_CoursesIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Course Name</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>\n" +
                "                                        <tbody>";
            for (var i in result){
                content += "<tr>";
                content += "<td>" + result[i].name + "</td>";
                content += "<td><a href='course-edit.html?courseId=" + result[i].courseId + "'> Edit </a>";
                content += "<a href='course-detail.html?courseId=" + result[i].courseId + "'>" + "Details </a>";
                content += "<a class='delete-course " + result[i].courseId + "'>" + "Delete </a>";
                content += "</td>";
                content += "</tr>";
            }
            content += "</tbody>\n" +
                "\n" +
                "                                    </table>";
            $("#listFaculties").html(content);
            $('#listFaculties').find('.table').DataTable();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });
});