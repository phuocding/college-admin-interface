$(document).ready(function () {
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_FacultiesIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "";
            for (var i in result){
                content += "<option value='" + result[i].facultyId + "'>" + result[i].name + "</option>"
            }
            $("#facultyList").html(content);
            $('#facultyList').multiselect({
                enableFiltering: true
            });
            console.log(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_CoursesIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "";
            for (var i in result){
                content += "<option value='" + result[i].courseId + "'>" + result[i].name + "</option>"
            }
            $("#courseList").html(content);
            $('#courseList').multiselect({
                enableFiltering: true
            });
            console.log(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $(".btn-submit").click(function (){
        if($("#createSubjectForm")[0].checkValidity()) {
            var formData = {
                "Name" : $("#createSubjectForm").find('input[name="Name"]').val(),
                "FacultiesIds" : $("#createSubjectForm").find('select[name="facultiesIds[]"]').val(),
                "CoursesIds" : $("#createSubjectForm").find('select[name="coursesIds[]"]').val(),
            };
            $.ajax({
                type: 'POST',
                accepts: 'application/json',
                contentType: 'application/json',
                url: API_CreateSubject,
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                data: JSON.stringify(formData),
                success: function (result) {
                    swal("Create New Subject Successful");
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (xhr.status == 409) {
                        alert("Subject Existed");
                    }
                }
            });
        }
        else {
            $("#createSubjectForm")[0].reportValidity();
        }
    });
});