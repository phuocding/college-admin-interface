$(document).ready(function () {
    generateTextEditor();
    $(".btn-submit").click(function (){
        if($("#createCourseForm")[0].checkValidity()) {
            var formData = {
                "Name" : $("#createCourseForm").find('input[name="Name"]').val(),
                "Description" : $("#createCourseForm").find('textarea[name="Description"]').val(),
                "DescriptionDetails" : $(".simditor-body")[0].innerHTML,
                "Price" : $("#createCourseForm").find('input[name="Price"]').val(),
                "ImageUrl" : $("#createCourseForm").find('input[name="image"]').val(),
        };
            $.ajax({
                type: 'POST',
                accepts: 'application/json',
                contentType: 'application/json',
                url: API_CreateCourse,
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                data: JSON.stringify(formData),
                success: function (result) {
                    swal("Create New Course Successful");
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (xhr.status == 409) {
                        alert("Course Existed");
                    }
                }
            });
        }
        else {
            $("#createCourseForm")[0].reportValidity();
        }
    });


    $('#add_images').on('propertychange input', function (e) {
        var valueChanged = false;
        if (e.type=='propertychange') {
            valueChanged = e.originalEvent.propertyName=='value';
        } else {
            valueChanged = true;
        }
        if (valueChanged) {
            $('.preview_images').html("");
            var imgUrl = $("#createCourseForm").find('input[name="image"]').val();
            $($.parseHTML('<img>')).attr('src', imgUrl).appendTo(".preview_images");
            $("img").addClass("preview_image");
            $('.preview_images').removeClass("hidden");
        }
    });

    $(":reset").click(function (){
        $(".preview_images").addClass('hidden');
        $(".preview_image").remove();
    });
});