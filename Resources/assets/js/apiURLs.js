var API_LOGIN = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/Login";
var API_AccountsIndex = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/AccountsIndex";
var API_ClazzsIndex = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/ClazzsIndex";
var API_SubjectsIndex = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/SubjectsIndex";
var API_CreateAccount = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CreateAccount";
var API_CreateClazz = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CreateClazz";
var API_CreateSubject = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CreateSubject";
var API_ClazzDetails = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/ClazzDetails";
var API_StudentDetails = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/StudentDetails";
var API_EditAccount = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/EditAccount";
var API_AddStudents = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/AddStudents";
var API_AddGrades = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/AddGrades";
var API_DeleteClazz = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteClazz";
var API_DeleteStudentFromClazz = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteStudentFromClazz";
var API_DeleteSubject = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteSubject";
var API_DeleteAccount = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteAccount";
var API_GetStudenClazzs = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/GetStudenClazzs";
var API_EditGrades = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/EditGrades";
var API_EditClazz = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/EditClazz";
var API_GetStudenGrades = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/GetStudenGrades";
var API_CreateDepartment = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CreateDepartment";
var API_CreateFaculty = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CreateFaculty";
var API_DepartmentsIndex = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DepartmentsIndex";
var API_FacultiesIndex = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/FacultiesIndex";
var API_DepartmentDetails = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DepartmentDetails";
var API_EditDepartment = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/EditDepartment";
var API_FacultyDetails = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/FacultyDetails";
var API_EditFaculty = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/EditFaculty";
var API_ApplicationApproval = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/ApplicationApproval";
var API_CreateCourse = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CreateCourse";
var API_CoursesIndex = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CoursesIndex";
var API_CourseDetails = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CourseDetails";
var API_EditCourse = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/EditCourse";
var API_DeleteFacultyFromDepartment = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteFacultyFromDepartment";
var API_AddFaculty = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/AddFaculty";
var API_DeleteClassFromFaculty = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteClassFromFaculty";
var API_AddClazz = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/AddClazz";
var API_DeleteSubjectFromFaculty = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteSubjectFromFaculty";
var API_AddSubjectToFaculty = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/AddSubjectToFaculty";
var API_AddClazzToCourse = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/AddClazzToCourse";
var API_AddSubjectToCourse = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/AddSubjectToCourse";
var API_DeleteClassFromCourse = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteClassFromCourse";
var API_DeleteSubjectFromCourse = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteSubjectFromCourse";
var API_CreatePost = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CreatePost";
var API_PostIndex = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/PostIndex";
var API_EditPost = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/EditPost";
var API_PostDetails = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/PostDetails";
var API_DeletePost = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeletePost";
var API_SendMultipleEmail = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/SendMultipleEmail";
var API_DeleteDepartment = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteDepartment";
var API_DeleteCourse = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteCourse";
var API_DeleteFaculty = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/DeleteFaculty";
var API_CourseSubjectExamDate = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/CourseSubjectExamDate";
var API_FacultySubjectExamDate = "https://studentresourcesapi.azurewebsites.net/api/CollageAdminAPI/FacultySubjectExamDate";

// delete account from template
$(document).on('click', '.delete-student' ,function(){
  if(confirm("Delete this Student?")){
      var accountId = $(this).attr("class").split(' ')[1];
      $.ajax({
          accepts: 'application/json',
          contentType: 'application/json',
          type: 'POST',
          headers: {
              "Authorization": Cookies.get("token"),
              "Role": Cookies.get("loggedUserRole"),
          },
          url: API_DeleteAccount,
          data: JSON.stringify(accountId),
          success: function (result) {
              alert("Deleted");
              window.location.reload();

          },
          error: function (xhr, textStatus, errorThrown) {
          }
      });
  }
});

$(document).on('click', '.delete-department' ,function(){
    if(confirm("Delete this department?")){
        var departmentId = $(this).attr("class").split(' ')[1];
        $.ajax({
            accepts: 'application/json',
            contentType: 'application/json',
            type: 'POST',
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            url: API_DeleteDepartment,
            data: JSON.stringify(departmentId),
            success: function (result) {
                alert("Deleted");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    }
});

$(document).on('click', '.delete-course' ,function(){
    if(confirm("Delete this course?")){
        var courseId = $(this).attr("class").split(' ')[1];
        $.ajax({
            accepts: 'application/json',
            contentType: 'application/json',
            type: 'POST',
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            url: API_DeleteCourse,
            data: JSON.stringify(courseId),
            success: function (result) {
                alert("Deleted");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    }
});

$(document).on('click', '.delete-faculty' ,function(){
    if(confirm("Delete this faculty?")){
        var facultyId = $(this).attr("class").split(' ')[1];
        $.ajax({
            accepts: 'application/json',
            contentType: 'application/json',
            type: 'POST',
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            url: API_DeleteFaculty,
            data: JSON.stringify(facultyId),
            success: function (result) {
                alert("Deleted");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    }
});

$(document).on('click', '.delete-post' ,function(){
    if(confirm("Delete this post?")){
        var postId = $(this).attr("class").split(' ')[1];
        $.ajax({
            accepts: 'application/json',
            contentType: 'application/json',
            type: 'POST',
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            url: API_DeletePost,
            data: JSON.stringify(postId),
            success: function (result) {
                alert("Deleted");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    }
});

$(document).on('click', '.delete-student-from-class' ,function(){
    if(confirm("Delete this Student From Class?")){
        var studentClazz = {
            "AccountId" : $(this).attr("class").split(' ')[1],
            "ClazzId" : window.location.href.split("=")[1],
        };
        $.ajax({
            accepts: 'application/json',
            contentType: 'application/json',
            type: 'POST',
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            url: API_DeleteStudentFromClazz,
            data: JSON.stringify(studentClazz),
            success: function (result) {
                alert("Deleted Student From Class");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    }
});

$(document).on('click', '.delete-subject' ,function(){
    if(confirm("Delete this Subject?")){
        var subjectId = $(this).attr("class").split(' ')[1];
        $.ajax({
            accepts: 'application/json',
            contentType: 'application/json',
            type: 'POST',
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            url: API_DeleteSubject,
            data: JSON.stringify(subjectId),
            success: function (result) {
                alert("Deleted");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    }
});