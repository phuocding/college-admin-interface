$(document).ready(function () {
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_AccountsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                    <thead>\n" +
                "                                    <tr>\n" +
                "                                        <th>Roll Number</th>\n" +
                "                                        <th>Name</th>\n" +
                "                                        <th>Phone</th>\n" +
                "                                        <th>Email</th>\n" +
                "                                        <th>Status</th>\n" +
                "                                        <th>Roles</th>\n" +
                "                                        <th>Action</th>\n" +
                "                                    </tr>\n" +
                "                                    </thead>\n" +
                "                                    <tbody>";
            for (var i in result.studentAccounts){
                if(result.studentAccounts[i].account.approvalStatus == 2){
                    content += "<tr>";
                    content += "<td>" + result.studentAccounts[i].account.rollNumber + "</td>";
                    content += "<td>" + result.studentAccounts[i].account.generalInformation.name + "</td>";
                    content += "<td>" + result.studentAccounts[i].account.generalInformation.phone + "</td>";
                    content += "<td>" + result.studentAccounts[i].account.generalInformation.email + "</td>";
                    content += "<td>" + result.studentAccounts[i].account.status + "</td>";
                    content += "<td><ul>";
                    content += "<li>" + result.studentAccounts[i].role.name + "</li>";
                    content += "</ul></td>";
                    content += "<td><a href='account-edit.html?studentId=" + result.studentAccounts[i].account.accountId + "'> Edit </a>";
                    content += "<a href='student-detail.html?studentId=" + result.studentAccounts[i].account.accountId + "'>" + " Details </a>";
                    content += "<a class='delete-student " + result.studentAccounts[i].account.accountId + "'>" + " Delete </a>";
                    content += "</td>";
                    content += "</tr>";
                }
            }
            content += "</tbody>\n" +
                "\n" +
                "                                </table>";
            $("#studentList").html(content);
            $('#studentList').find('.table').DataTable();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });
});