$(document).ready(function () {
    generateTextEditor();
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_DepartmentsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<option value=\"\" disabled selected>Select Department</option>\n";
            for (var i in result){
                content += "<option value='" + result[i].departmentId + "'>" + result[i].name + "</option>"
            }
            $("#departmentList").html(content);
            console.log(result)
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $(".btn-submit").click(function (){
        if($("#createFacultyForm")[0].checkValidity()) {
            var formData = {
                "Name" : $("#createFacultyForm").find('input[name="Name"]').val(),
                "Description" : $("#createFacultyForm").find('textarea[name="Description"]').val(),
                "DescriptionDetails" : $(".simditor-body")[0].innerHTML,
                "DepartmentId" : $("#createFacultyForm").find('select[name="departmentId"]').val(),
                "ImageUrl" : $("#createFacultyForm").find('input[name="image"]').val(),
            };
            $.ajax({
                type: 'POST',
                accepts: 'application/json',
                contentType: 'application/json',
                url: API_CreateFaculty,
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                data: JSON.stringify(formData),
                success: function (result) {
                    swal("Create New Faculty Successful");
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (xhr.status == 409) {
                        alert("Faculty Existed");
                    }
                }
            });
        }
        else {
            $("#createFacultyForm")[0].reportValidity();
        }
    });

    $('#add_images').on('propertychange input', function (e) {
        var valueChanged = false;
        if (e.type=='propertychange') {
            valueChanged = e.originalEvent.propertyName=='value';
        } else {
            valueChanged = true;
        }
        if (valueChanged) {
            $('.preview_images').html("");
            var imgUrl = $("#createFacultyForm").find('input[name="image"]').val();
            $($.parseHTML('<img>')).attr('src', imgUrl).appendTo(".preview_images");
            $("img").addClass("preview_image");
            $('.preview_images').removeClass("hidden");
        }
    });

    $(":reset").click(function (){
        $(".preview_images").addClass('hidden');
        $(".preview_image").remove();
    });
});