function getApplicationList(){
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_AccountsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                    <thead>\n" +
                "                                    <tr>\n" +
                "                                        <th>Roll Number</th>\n" +
                "                                        <th>Name</th>\n" +
                "                                        <th>Phone</th>\n" +
                "                                        <th>Email</th>\n" +
                "                                        <th>Approval Status</th>\n" +
                "                                        <th>Action</th>\n" +
                "                                    </tr>\n" +
                "                                    </thead>\n" +
                "                                    <tbody>";
            for (var i in result.studentAccounts){
                content += "<tr class='listData'>";
                content += "<td>" + result.studentAccounts[i].account.rollNumber + "</td>";
                content += "<td>" + result.studentAccounts[i].account.generalInformation.name + "</td>";
                content += "<td>" + result.studentAccounts[i].account.generalInformation.phone + "</td>";
                content += "<td>" + result.studentAccounts[i].account.generalInformation.email + "</td>";
                if(result.studentAccounts[i].account.approvalStatus == 0){
                    content += "<td class='status-declined'><i class='fa fa-ban'> Declined </i></td>"
                }
                else if(result.studentAccounts[i].account.approvalStatus == 1){
                    content += "<td class='status-pending'><i class='fa fa-clock-o'> Pending </i></td>"
                }
                else if(result.studentAccounts[i].account.approvalStatus == 2){
                    content += "<td class='status-accepted'><i class='fa fa-check-circle-o'> Accepted </i></td>"
                }
                if(result.studentAccounts[i].account.approvalStatus == 1){
                    content += "<td class='actions' id='" + result.studentAccounts[i].account.accountId + "'><span class='fa fa-check-circle-o accept application'> Accept </span>";
                    content += "<span class='fa fa-ban decline application'> Decline </span>";
                    content += "</td>";
                }
                else if(result.studentAccounts[i].account.approvalStatus == 2){
                    content += "<td class='actions' id='" + result.studentAccounts[i].account.accountId + "'><span class='fa fa-clock-o pending application'> Pending </span>";
                    content += "<span class='fa fa-ban decline application'> Decline </span>";
                    content += "</td>";
                }
                else if(result.studentAccounts[i].account.approvalStatus == 0){
                    content += "<td class='actions' id='" + result.studentAccounts[i].account.accountId + "'><span class='fa fa-check-circle-o accept application'> Accept </span>";
                    content += "<span class='fa fa-clock-o pending application'> Pending </span>";
                    content += "</td>";
                }
                content += "</tr>";
            }
            content += "</tbody>\n" +
                "\n" +
                "                                </table>";
            $("#studentList").html(content);
            $('#studentList').find('.table').DataTable();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });
}
$(document).ready(function () {
    getApplicationList();

    $("#approvalStatusFilter").on('change' , function () {
        if($(this).val() == "pending"){
            $(".listData").show();
            $(".listData").not($(".status-pending").closest("tr")).hide();
        }
        if($(this).val() == "accepted"){
            $(".listData").show();
            $(".listData").not($(".status-accepted").closest("tr")).hide();
        }
        if($(this).val() == "declined"){
            $(".listData").show();
            $(".listData").not($(".status-declined").closest("tr")).hide();
        }
        if($(this).val() == "all"){
            $(".listData").show();
        }
    });

    $(document).on('click', '.application' ,function () {
        if(confirm("Do you want to" + $(this).text().toLowerCase() + "this applicant?")){
            var studentIds = [];
            studentIds.push($(this).closest("td").attr('id'));
            if($(this).hasClass('decline')){
                studentIds.push(0)
            }
            else if($(this).hasClass('pending')){
                studentIds.push(1)
            }
            else if($(this).hasClass('accept')){
                studentIds.push(2)
            }
            $.ajax({
                type: 'POST',
                accepts: 'application/json',
                contentType: 'application/json',
                url: API_ApplicationApproval,
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                data: JSON.stringify(studentIds),
                success: function (result){
                    swal("Successful")
                    getApplicationList();
                },
                error: function (xhr, textStatus, errorThrown) {
                    alert("error");
                }
            });
        }
    });
});