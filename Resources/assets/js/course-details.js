$(document).ready(function () {
    $("#userName").text("Welcome " + Cookies.get("loggedUserName"));
    $("#viewClassBtn").click(function () {
        $("#classList").show();
        $("#listSubjects").hide();
});

    $("#viewSubjectBtn").click(function () {
        $("#listSubjects").show();
        $("#classList").hide();
    });

    $("#addDate").click(function () {
        $(".multiselect-native-select").show();
        $(".choose-date").show();
        $(".btn-submit-date").show();
        $(this).hide();
    });

    $.ajax({
        type: 'POST',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_CourseDetails,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        data: JSON.stringify(window.location.href.split("=")[1]),
        success: function (result) {
            $("#courseName").text(result.course[0].name);
            $("#facultyPrice").text(result.course[0].price + "$");
            $("#courseDescription").text(result.course[0].description);
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\" id=\"example\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Class Name</th>\n" +
                "                                                <th>Teacher</th>\n" +
                "                                                <th>Created At</th>\n" +
                "                                                <th>Updated At</th>\n" +
                "                                                <th>Status</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>\n" +
                "                                        <tbody>";
            for (var i in result.course[0].clazz){
                content += "<tr>";
                content += "<td>" + result.course[0].clazz[i].name + "</td>";
                content += "<td>" + result.course[0].clazz[i].teacher + "</td>";
                content += "<td>" + result.course[0].clazz[i].createdAt + "</td>";
                content += "<td>" + result.course[0].clazz[i].updatedAt + "</td>";
                content += "<td>" + result.course[0].clazz[i].status + "</td>";
                content += "<td><a href='class-edit.html?classId=" + result.course[0].clazz[i].clazzId + "'> Edit </a>";
                content += "<a href='class-detail.html?classId=" + result.course[0].clazz[i].clazzId + "'>" + "Details </a>";
                content += "<a class='delete-class " + result.course[0].clazz[i].clazzId + "'>" + "Delete </a><br>";
                content += "<a class='delete-class-from-course " + result.course[0].clazz[i].clazzId + "'>" + " Delete From Course </a>";
            }
            content += "</tbody>\n" +
                "\n" +
                "                                    </table>";
            $("#classList").html(content);
            $('#classList').find('.table').DataTable();

            var content1 = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Subject Name</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>";
            for (var i in result.courseSubjects){
                content1 += "<tr>";
                content1 += "<td>" + result.courseSubjects[i].subject.name + "</td>";
                content1 += "<td><a href='#'> Edit </a>";
                content1 += "<a class='delete-subject " + result.courseSubjects[i].subject.subjectId + "'>" + "Delete </a>";
                content1  += "<a class='delete-subject-from-course " + result.courseSubjects[i].subject.subjectId + "'>" + " Delete From Course </a>";
                content1 += "</td>";
                content1 += "</tr>";
            }
            content1 += "</tbody>\n" +
                "\n" +
                "                                    </table>";
            $("#listSubjects").html(content1);
            $('#listSubjects').find('.table').DataTable();
            console.log(result);

        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $.ajax({
        type: 'POST',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_CourseDetails,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        data: JSON.stringify(window.location.href.split("=")[1]),
        success: function (result) {
            var content = "";
            for (var i in result.courseSubjects){
                    content += "<option value='" + result.courseSubjects[i].subject.subjectId + "'>" + result.courseSubjects[i].subject.name + "</option>"
            }
            $("#addExamDate").html(content);
            $('#addExamDate').multiselect({
                enableFiltering: true,
                disableIfEmpty: true,
            });
            $(".multiselect-native-select").eq(2).hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $(".btn-submit-date").click(function () {
        var formData = {
            "CourseId" : window.location.href.split("=")[1],
            "SubjectIds" : $("#addExamDate").val(),
            "ExamDate" : $("#chooseDate").val(),
        };
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_CourseSubjectExamDate,
            headers: {
                "Authorization": Cookies.get("token"),
            },
            data: JSON.stringify(formData),
            success: function (result) {
                alert("Added Exam Date Successful");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
    });

    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_ClazzsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "";
            for (var i in result){
                if(result[i].inCourseStatus == 0 && result[i].inFacultyStatus == 0){
                    content += "<option value='" + result[i].clazzId + "'>" + result[i].name + "</option>"
                }
            }
            $("#addClassList").html(content);
            $('#addClassList').multiselect({
                enableFiltering: true,
                disableIfEmpty: true,
            });
            $( document ).ajaxStop(function() {
                if($('#addClassList').has('option').length < 0 ) {
                    $(".text-danger").show();
                }
                else if($('#addClassList').has('option').length > 0){
                    $(".text-danger").hide();
                }
            });

            },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_SubjectsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "";
            for (var i in result){
                    content += "<option value='" + result[i].subjectId + "'>" + result[i].name + "</option>"
            }
            $("#addSubjectList").html(content);
            $('#addSubjectList').multiselect({
                enableFiltering: true,
                disableIfEmpty: true,
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $(".btn-submit-class").click(function (){
        var formData = {
            "CourseId" : window.location.href.split("=")[1],
            "ClazzIds" : $("#addClassList").val()
        };
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_AddClazzToCourse,
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole")
            },
            data: JSON.stringify(formData),
            success: function (result) {
                alert("Added Classes Successful");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
    });

    $(".btn-submit-subject").click(function (){
        var formData = {
            "CourseId" : window.location.href.split("=")[1],
            "SubjectIds" : $("#addSubjectList").val()
        };
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_AddSubjectToCourse,
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole")
            },
            data: JSON.stringify(formData),
            success: function (result) {
                alert("Added Subjects Successful");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
    });

    $(document).on('click', '.delete-class-from-course' ,function(){
        if(confirm("Delete this class from this course?")){
            var class_courseId = [];
            class_courseId.push($(this).attr("class").split(' ')[1]);
            class_courseId.push(window.location.href.split("=")[1]);
            $.ajax({
                accepts: 'application/json',
                contentType: 'application/json',
                type: 'POST',
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                url: API_DeleteClassFromCourse,
                data: JSON.stringify(class_courseId),
                success: function (result) {
                    alert("Deleted class from course");
                    window.location.reload();
                },
                error: function (xhr, textStatus, errorThrown) {
                }
            });
        }
    });

    $(document).on('click', '.delete-subject-from-course' ,function(){
        if(confirm("Delete this subject from this course?")){
            var formData = {
                "CourseId" : window.location.href.split("=")[1],
                "SubjectId" : $(this).attr("class").split(' ')[1]
            };
            $.ajax({
                accepts: 'application/json',
                contentType: 'application/json',
                type: 'POST',
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                url: API_DeleteSubjectFromCourse,
                data: JSON.stringify(formData),
                success: function (result) {
                    alert("Deleted subject from course");
                    window.location.reload();
                },
                error: function (xhr, textStatus, errorThrown) {
                }
            });
            console.log(subject_facultyId)
        }
    });
});