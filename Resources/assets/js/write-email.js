var editor = new Simditor({
    textarea: $('#editor'),
    toolbar:
        [
            'title',
            'bold',
            'italic',
            'underline',
            'strikethrough',
            'fontScale',
            'color',
            'ol',
            'ul' ,
            'blockquote',
            'code'  ,
            'table',
            'link',
            'image',
            'hr'    ,
            'indent',
            'outdent',
            'alignment',
        ]
});

$.ajax({
    type: 'GET',
    accepts: 'application/json',
    contentType: 'application/json',
    url: API_AccountsIndex,
    headers: {
        "Authorization": Cookies.get("token"),
    },
    success: function (result) {
        var content = "";
        for (var i in result.clazzs) {
            if (result.clazzs[i].studentClazzs.length != 0) {
                content += "<optgroup label='" + result.clazzs[i].name + "'>";
                for (var j in result.studentClazzs) {
                    if (result.studentClazzs[j].clazz.clazzId == result.clazzs[i].clazzId) {
                        content += "<option value='" + result.studentClazzs[j].account.accountId + "'>";
                        content += result.studentClazzs[j].account.generalInformation.name + "</option>"
                    }
                    content += "</optgroup>"
                }
            }
        }
        $("#select-student").html(content);
        $('#select-student').multiselect({
            enableFiltering: true,
            disableIfEmpty: true,
            enableClickableOptGroups: true
        });
    },
    error: function (xhr, textStatus, errorThrown) {
        alert("error");
    }
});

$(".btn-submit").click(function (){
    if($("#createEmailForm")[0].checkValidity()) {
        var formData = {
            "Subject" : $("#createEmailForm").find('input[name="subject"]').val(),
            "StudentIds" : $("#select-student").val(),
            "Content" : $(".simditor-body")[0].innerHTML,
        };
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_SendMultipleEmail,
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            data: JSON.stringify(formData),
            success: function (result) {
                swal("Sent Successful");
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
        console.log(formData)
    }
    else {
        $("#createEmailForm")[0].reportValidity();
    }
});