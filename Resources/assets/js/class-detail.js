$(document).ready(function () {
    //**********************

    //lấy về danh sách subject, student trong lớp => cho vào bảng subject, student và select subject lớp đang học để thêm điểm
    $.ajax({
        type: 'POST',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_ClazzDetails,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        data: JSON.stringify(window.location.href.split("=")[1]),
        success: function (result) {
            $(".className").text("Class: " + result.clazz[0].name);
            $(".teacherName").text(" | Teacher: " + result.clazz[0].teacher);
            if(result.clazz[0].faculty != null){
                $(".facultyName").text(" | Faculty: " + result.clazz[0].faculty.name);
                var content3 = "<option value=\"\" disabled selected>Select Subject</option>\n";
                for(var i in result.clazz[0].faculty.facultySubjects){
                    content3 += "<option value='" + result.clazz[0].faculty.facultySubjects[i].subject.subjectId + "'>" + result.clazz[0].faculty.facultySubjects[i].subject.name + "</option>"
                }
                $("#select-subject-grade").html(content3);
            }
            if(result.clazz[0].course != null){
                $(".courseName").text(" | Course: " + result.clazz[0].course.name);
                content3 = "<option value=\"\" disabled selected>Select Subject</option>\n";
                for(var i in result.clazz[0].course.courseSubjects){
                    content3 += "<option value='" + result.clazz[0].course.courseSubjects[i].subject.subjectId + "'>" + result.clazz[0].course.courseSubjects[i].subject.name + "</option>"
                }
                $("#select-subject-grade").html(content3);
            }
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Roll Number</th>\n" +
                "                                                <th>Name</th>\n" +
                "                                                <th>Phone</th>\n" +
                "                                                <th>Email</th>\n" +
                "                                                <th>Status</th>\n" +
                "                                                <th>Roles</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                                <th class=\"gradeForms-title hidden\">Grade</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>\n" +
                "                                        <tbody>";
            for (var i in result.studentClazz){
                if(result.studentClazz[i].account.status != 0 && result.studentClazz[i].status != 0 && result.studentClazz[i].account.approvalStatus == 2){
                    content += "<tr>";
                    content += "<td>" + result.studentClazz[i].account.rollNumber + "</td>";
                    content += "<td>" + result.studentClazz[i].account.generalInformation.name + "</td>";
                    content += "<td>" + result.studentClazz[i].account.generalInformation.phone + "</td>";
                    content += "<td>" + result.studentClazz[i].account.generalInformation.email + "</td>";
                    content += "<td>" + result.studentClazz[i].account.status + "</td>";
                    content += "<td><ul>";
                    for(var j in result.studentClazz[i].account.roleAccounts){
                        content += "<li>" + result.studentClazz[i].account.roleAccounts[j].role.name + "</li>";
                    }
                    content += "</ul></td>";
                    content += "<td><a href='account-edit.html?studentId=" + result.studentClazz[i].account.accountId + "'>" + "Edit </a>";
                    content += "<a href='student-detail.html?studentId=" + result.studentClazz[i].account.accountId + "'>" + "Details </a>";
                    content += "<a class='delete-student " + result.studentClazz[i].account.accountId + "'>" + "Delete </a>";
                    content += "<a class='delete-student-from-class " + result.studentClazz[i].account.accountId + "'>" + " Delete From Class </a>";
                    content += "<td class='gradeForms hidden'>";
                    content += "<form class='gradeForm'>";
                    content += "<div class='form-group'>";
                    content += "<input type='hidden' name='AccountId' value='" + result.studentClazz[i].accountId + "'>";
                    content += "<input type='hidden' class='subjectId' name='SubjectId'>";
                    content += "</div>";
                    content += "<div class='form-group'>";
                    content += "<label> Assignment Grade </label>";
                    content += "<input type='number' class='form-control' min='0' max='10' name='AssignmentGrade'>";
                    content += "</div>";
                    content += "<div class='form-group'>";
                    content += "<label> Practical Grade </label>";
                    content += "<input type='number' class='form-control' min='0' max='15' name='PraticalGrade'>";
                    content += "</div>";
                    content += "<div class='form-group'>";
                    content += "<label> Theoretical Grade </label>";
                    content += "<input type='number' class='form-control' min='0' max='10' name='TheoricalGrade'>";
                    content += "</div>";
                    content += "</form>";
                    content += "</td>";
                }
            }
            content += "</tbody>\n" +
                "                                        <tr class=\"gradeForms hidden\">\n" +
                "                                            <td><p class=\"hidden\">zzzzzzz</p></td>\n" +
                "                                            <td></td>\n" +
                "                                            <td></td>\n" +
                "                                            <td></td>\n" +
                "                                            <td></td>\n" +
                "                                            <td></td>\n" +
                "                                            <td></td>\n" +
                "                                            <td class=\"text-right\">\n" +
                "                                                <button class=\"btn btn-primary btn-submit-grade\">Save</button>\n" +
                "                                            </td>\n" +
                "                                        </tr>\n" +
                "                                    </table>";
            $("#studentList").html(content);
            $('#studentList').find('.table').DataTable();
            console.log(result);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    //**********************


    // Lấy về danh sách student để thêm vào lớp
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_AccountsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "";
            for (var i in result.studentAccounts){
                if(result.studentAccounts[i].account.status != 0  && result.studentAccounts[i].account.approvalStatus == 2){
                    content += "<option value='" + result.studentAccounts[i].accountId + "'>" + result.studentAccounts[i].account.generalInformation.name + "</option>"
                }
            }
            $("#select-student").html(content);
            $('#select-student').multiselect({
                enableFiltering: true,
                disableIfEmpty: true,
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    //**********************
    //**********************


    //đẩy ajax lên thêm student vào lớp
    $(".btn-save-student").click(function () {
        var chosenStudents = $("#select-student").val();
        chosenStudents.push(window.location.href.split("=")[1]);
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_AddStudents,
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            data: JSON.stringify(chosenStudents),
            success: function (result) {
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
    });
    //**********************


    //Thêm điểm cho nhiều student
    $(".add-grade-btn").on('click' , function () {
        $(this).addClass("hidden");
        $(".btn-save-student").addClass("hidden");
        $(".subject-grade").removeClass("hidden");
    });

    var subjectId = "";

    $(".subject-grade").on('change' , function () {
        subjectId = $(this).val();
        $(".gradeForms").removeClass("hidden");
        $(".gradeForms-title").removeClass("hidden");
    });

    $(document).on("click", ".btn-submit-grade", function () {
        for(var i = 0; i < $(".gradeForm").length; i++){
            if($(".gradeForm")[i].checkValidity() == false){
                $(".gradeForm")[i].reportValidity();
                return false;
            }
        }
        $(".subjectId").each(function () {
            $(this).val($("#select-subject-grade").val());
        });
        var grades = [];
        $(".gradeForm").each(function () {
            var formData = {
                'AccountId': $(this).find('input[name="AccountId"]').val(),
                'SubjectId': $(this).find('input[name="SubjectId"]').val(),
                'AssignmentGrade': $(this).find('input[name="AssignmentGrade"]').val(),
                'PracticalGrade': $(this).find('input[name="PraticalGrade"]').val(),
                'TheoreticalGrade': $(this).find('input[name="TheoricalGrade"]').val(),
            };
            grades.push(formData);
        });
        grades = JSON.stringify(grades);
        $.ajax({
            accepts: 'application/json',
            contentType: 'application/json',
            type: 'POST',
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            url: API_AddGrades,
            data: grades,
            success: function (result) {
                swal("Add grade Successful");
                $(".gradeForms").addClass("hidden");
                $(".subject-grade").addClass("hidden");
                $(".gradeForms-title").addClass("hidden");
                $(".add-grade-btn").removeClass("hidden");
                $(".btn-save-student").removeClass("hidden");
            },
            error: function (xhr, textStatus, errorThrown) {
                if (xhr.status == 409) {
                    swal("Students already had grades for this subject");
                }
            }
        });
    });
    //**********************
});