$(document).ready(function () {
    $("#userName").text("Welcome " + Cookies.get("loggedUserName"));
    function getFacultyList(){
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_DepartmentDetails,
            headers: {
                "Authorization": Cookies.get("token"),
            },
            data: JSON.stringify(window.location.href.split("=")[1]),
            success: function (result) {
                $("#departmentName").text(result[0].name);
                $("#departmentDescription").text(result[0].description);
                var content = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                    "                                        <thead>\n" +
                    "                                            <tr>\n" +
                    "                                                <th>Faculty Name</th>\n" +
                    "                                                <th>Action</th>\n" +
                    "                                            </tr>\n" +
                    "                                        </thead>\n" +
                    "                                        <tbody>";
                for (var i in result[0].faculties){
                    content += "<tr>";
                    content += "<td>" + result[0].faculties[i].name + "</td>";
                    content += "<td><a href='faculty-edit.html?facultyId=" + result[0].faculties[i].facultyId + "'> Edit </a>";
                    content += "<a href='faculty-detail.html?facultyId=" + result[0].faculties[i].facultyId + "'>" + "Details </a>";
                    content += "<a class='delete-faculty " + result[0].faculties[i].facultyId + "'>" + "Delete </a>";
                    content += "<a class='delete-faculty-from-department " + result[0].faculties[i].facultyId + "'>" + " Delete From Department </a>";
                    content += "</td>";
                    content += "</tr>";
                }
                content += "</tbody>\n" +
                    "\n" +
                    "                                    </table>";
                $("#listFaculties").html(content);
                $('#listFaculties').find('.table').DataTable();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
    }

    function getSelectFacultyList(){
        $.ajax({
            type: 'GET',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_FacultiesIndex,
            headers: {
                "Authorization": Cookies.get("token"),
            },
            success: function (result) {
                var content = "";
                for (var i in result){
                    if(result[i].inDepartmentStatus == 0){
                        content += "<option value='" + result[i].facultyId + "'>" + result[i].name + "</option>"
                    }
                }
                $("#facultyList").html(content);
                $('#facultyList').multiselect({
                    enableFiltering: true,
                    disableIfEmpty: true,
                });
                $( document ).ajaxStop(function() {
                    if($('#facultyList').has('option').length < 0 ) {
                        $(".text-danger").show();
                    }
                    else if($('#facultyList').has('option').length > 0){
                        $(".text-danger").hide();
                    }
                });
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
    }

    getSelectFacultyList();
    getFacultyList();

    $(".btn-submit").click(function (){
        var formData = {
            "DepartmentId" : window.location.href.split("=")[1],
            "FacultiesIds" : $("#facultyList").val()
        };
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_AddFaculty,
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole")
            },
            data: JSON.stringify(formData),
            success: function (result) {
                alert("Added Faculties Successful");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
    });

    $(document).on('click', '.delete-faculty-from-department' ,function(){
        if(confirm("Delete this faculty from this department?")){
            var faculty_departmentId = [];
            faculty_departmentId.push($(this).attr("class").split(' ')[1]);
            faculty_departmentId.push(window.location.href.split("=")[1]);
            $.ajax({
                accepts: 'application/json',
                contentType: 'application/json',
                type: 'POST',
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                url: API_DeleteFacultyFromDepartment,
                data: JSON.stringify(faculty_departmentId),
                success: function (result) {
                    alert("Deleted Faculty From Class");
                    window.location.reload();
                },
                error: function (xhr, textStatus, errorThrown) {
                }
            });
        }
    });
});