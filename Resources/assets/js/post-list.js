$(document).ready(function () {
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_PostIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Post Thumbnail</th>\n" +
                "                                                <th>Post Name</th>\n" +
                "                                                <th class='description'>Post Description</th>\n" +
                "                                                <th>Created Date</th>\n" +
                "                                                <th>Edited Date</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>\n" +
                "                                        <tbody>";
            for (var i in result){
                content += "<tr>";
                content += "<td><img src='" + result[i].imageUrl + "'></td>";
                content += "<td>" + result[i].title + "</td>";
                content += "<td class='description'>" + result[i].description + "</td>";
                content += "<td>" + convertDateTime(result[i].createdAt) + "</td>";
                content += "<td>" + convertDateTime(result[i].updatedAt) + "</td>";
                content += "<td><a href='edit-post.html?postId=" + result[i].postId + "'> Edit </a>";
                content += "<a class='delete-post " + result[i].postId + "'>" + "Delete </a>";
                content += "</td>";
                content += "</tr>";
            }
            content += "</tbody>\n" +
                "\n" +
                "                                    </table>";
            $("#listPosts").html(content);
            $('#listPosts').find('.table').DataTable({
                "columnDefs": [
                    { "width": "10px", "targets": 0 },
                    { "width": "40px", "targets": 1 },
                    { "width": "100px", "targets": 2 },
                    { "width": "70px", "targets": 3 },
                    { "width": "70px", "targets": 4 },
                    { "width": "70px", "targets": 5 }
                ],
            }).columns.adjust();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });
});