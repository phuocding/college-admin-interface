$(document).ready(function () {
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_DepartmentsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Department Name</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>\n" +
                "                                        <tbody>";
            for (var i in result){
                content += "<tr>";
                content += "<td>" + result[i].name + "</td>";
                content += "<td><a href='department-edit.html?departmentId=" + result[i].departmentId + "'> Edit </a>";
                content += "<a href='department-detail.html?departmentId=" + result[i].departmentId + "'>" + "Details </a>";
                content += "<a class='delete-department " + result[i].departmentId + "'>" + "Delete </a>";
                content += "</td>";
                content += "</tr>";
            }
            content += "</tbody>\n" +
                "\n" +
                "                                    </table>";
            $("#listDepartment").html(content);
            $('#listDepartment').find('.table').DataTable();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });
});