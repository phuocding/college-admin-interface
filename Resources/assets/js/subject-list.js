$(document).ready(function () {
    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_SubjectsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Subject Name</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>";
            for (var i in result){
                content += "<tr>";
                content += "<td>" + result[i].name + "</td>";
                content += "<td><a href='#'> Edit </a>";
                content += "<a class='delete-subject " + result[i].subjectId + "'>" + "Delete </a>";
                content += "</td>";
                content += "</tr>";
            }
            content += "</tbody>\n" +
                "\n" +
                "                                    </table>";
            $("#listSubjects").html(content);
            $('#listSubjects').find('.table').DataTable();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });
});