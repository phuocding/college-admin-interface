generateTextEditor();

$.ajax({
    type: 'POST',
    accepts: 'application/json',
    contentType: 'application/json',
    url: API_PostDetails,
    data: JSON.stringify(window.location.href.split("=")[1]),
    success: function (result) {
        $("#editForm").find('input[name="title"]').val(result.title);
        $("#editForm").find('textarea[name="description"]').val(result.description);
        $("#editForm").find('input[name="image"]').val(result.imageUrl);
        $(".post-status").each(function () {
            if(result.status == $(this).val()){
                $(this).prop('checked', true);
            }
        });
        $($.parseHTML('<img>')).attr('src', result.imageUrl).appendTo(".preview_images");
        $("img").addClass("preview_image");
        $('.preview_images').removeClass("hidden");
        $(".simditor-body").eq(0).html(result.content);
    },
    error: function (xhr, textStatus, errorThrown) {
        alert("error");
    }
});

$('#add_images').on('propertychange input', function (e) {
    var valueChanged = false;
    if (e.type=='propertychange') {
        valueChanged = e.originalEvent.propertyName=='value';
    } else {
        valueChanged = true;
    }
    if (valueChanged) {
        $('.preview_images').html("");
        var imgUrl = $("#createPostForm").find('input[name="image"]').val();
        $($.parseHTML('<img>')).attr('src', imgUrl).appendTo(".preview_images");
        $("img").addClass("preview_image");
        $('.preview_images').removeClass("hidden");
    }
});

$(".btn-submit").click(function (){
    if($("#editForm")[0].checkValidity()) {
        var formData = {
            "Title" : $("#editForm").find('input[name="title"]').val(),
            "Description" : $("#editForm").find('textarea[name="description"]').val(),
            "Content" : $(".simditor-body")[0].innerHTML,
            "ImageUrl" : $("#editForm").find('input[name="image"]').val(),
            "PostId": window.location.href.split("=")[1],
        };
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_EditPost,
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole"),
            },
            data: JSON.stringify(formData),
            success: function (result) {
                swal("Edit Post Successful");
            },
            error: function (xhr, textStatus, errorThrown) {
            }
        });
    }
    else {
        $("#editForm")[0].reportValidity();
    }
});