$(document).ready(function () {

    $(".btn-submit").click(function (){
        if($("#createClassForm")[0].checkValidity()) {
            var formData = {
                "Name" : $("#createClassForm").find('input[name="Name"]').val(),
                "Teacher" : $("#createClassForm").find('input[name="Teacher"]').val(),
                "Status" : $("#createClassForm").find('input[name="Status"]').val(),
                "FacultyId" : $("#createClassForm").find('select[name="facultyId"]').val(),
                "CourseId" : $("#createClassForm").find('select[name="courseId"]').val(),
            };
            $.ajax({
                type: 'POST',
                accepts: 'application/json',
                contentType: 'application/json',
                url: API_CreateClazz,
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                data: JSON.stringify(formData),
                success: function (result) {
                    swal("Create New Class Successful");
                },
                error: function (xhr, textStatus, errorThrown) {
                    if (xhr.status == 409) {
                        alert("Class Existed");
                    }
                }
            });
        }
        else {
            $("#createClassForm")[0].reportValidity();
        }
    });

    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_FacultiesIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<option value='0' selected>Select Faculty</option>\n";
            for (var i in result){
                content += "<option value='" + result[i].facultyId + "'>" + result[i].name + "</option>"
            }
            $("#facultyList").html(content);
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_CoursesIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "<option value='0' selected>Select Course</option>\n";
            for (var i in result){
                content += "<option value='" + result[i].courseId + "'>" + result[i].name + "</option>"
            }
            $("#courseList").html(content);
            console.log(result)
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $("#createClassForm").find('select[name="facultyId"]').on('change', function () {
        if($("#createClassForm").find('select[name="facultyId"]').val() != 0){
                $("#createClassForm").find('select[name="courseId"]').val(0);
        }
    });

    $("#createClassForm").find('select[name="courseId"]').on('change', function () {
        if($("#createClassForm").find('select[name="courseId"]').val() != 0){
            $("#createClassForm").find('select[name="facultyId"]').val(0);
        }
    })
});
