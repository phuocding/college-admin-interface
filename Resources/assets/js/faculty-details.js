$(document).ready(function () {
    $("#userName").text("Welcome " + Cookies.get("loggedUserName"));
    $("#viewClassBtn").click(function () {
        $("#classList").show();
        $("#listSubjects").hide();
});

    $("#viewSubjectBtn").click(function () {
        $("#listSubjects").show();
        $("#classList").hide();
    });

    $("#addDate").click(function () {
        $(".multiselect-native-select").show();
        $(".choose-date").show();
        $(".btn-submit-date").show();
        $(this).hide();
    });

    $.ajax({
        type: 'POST',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_FacultyDetails,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        data: JSON.stringify(window.location.href.split("=")[1]),
        success: function (result) {
            $("#facultyName").text(result.faculty[0].name);
            $("#facultyDepartment").text(result.faculty[0].department.name);
            $("#facultyDescription").text(result.faculty[0].description);
            var content = "<table class=\"table table-striped\" data-toggle=\"datatables\" id=\"example\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Class Name</th>\n" +
                "                                                <th>Teacher</th>\n" +
                "                                                <th>Created At</th>\n" +
                "                                                <th>Updated At</th>\n" +
                "                                                <th>Status</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>\n" +
                "                                        <tbody>";
            for (var i in result.faculty[0].clazzs){
                content += "<tr>";
                content += "<td>" + result.faculty[0].clazzs[i].name + "</td>";
                content += "<td>" + result.faculty[0].clazzs[i].teacher + "</td>";
                content += "<td>" + result.faculty[0].clazzs[i].createdAt + "</td>";
                content += "<td>" + result.faculty[0].clazzs[i].updatedAt + "</td>";
                content += "<td>" + result.faculty[0].clazzs[i].status + "</td>";
                content += "<td><a href='class-edit.html?classId=" + result.faculty[0].clazzs[i].clazzId + "'> Edit </a>";
                content += "<a href='class-detail.html?classId=" + result.faculty[0].clazzs[i].clazzId + "'>" + "Details </a>";
                content += "<a class='delete-class " + result.faculty[0].clazzs[i].clazzId + "'>" + "Delete </a><br>";
                content += "<a class='delete-class-from-faculty " + result.faculty[0].clazzs[i].clazzId + "'>" + " Delete From Faculty </a>";
            }
            content += "</tbody>\n" +
                "\n" +
                "                                    </table>";
            $("#classList").html(content);
            $('#classList').find('.table').DataTable();

            var content1 = "<table class=\"table table-striped\" data-toggle=\"datatables\">\n" +
                "                                        <thead>\n" +
                "                                            <tr>\n" +
                "                                                <th>Subject Name</th>\n" +
                "                                                <th>Action</th>\n" +
                "                                            </tr>\n" +
                "                                        </thead>";
            for (var i in result.facultySubjects){
                content1 += "<tr>";
                content1 += "<td>" + result.facultySubjects[i].subject.name + "</td>";
                content1 += "<td><a href='#'> Edit </a>";
                content1 += "<a class='delete-subject " + result.facultySubjects[i].subject.subjectId + "'>" + "Delete </a>";
                content1  += "<a class='delete-subject-from-faculty " + result.facultySubjects[i].subject.subjectId + "'>" + " Delete From Faculty </a>";
                content1 += "</td>";
                content1 += "</tr>";
            }
            content1 += "</tbody>\n" +
                "\n" +
                "                                    </table>";
            $("#listSubjects").html(content1);
            $('#listSubjects').find('.table').DataTable();
            console.log(result);

        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $.ajax({
        type: 'POST',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_FacultyDetails,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        data: JSON.stringify(window.location.href.split("=")[1]),
        success: function (result) {
            var content = "";
            for (var i in result.facultySubjects){
                content += "<option value='" + result.facultySubjects[i].subject.subjectId + "'>" + result.facultySubjects[i].subject.name + "</option>"
            }
            $("#addExamDate").html(content);
            $('#addExamDate').multiselect({
                enableFiltering: true,
                disableIfEmpty: true,
            });
            $(".multiselect-native-select").eq(2).hide();
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_ClazzsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "";
            for (var i in result){
                if(result[i].inFacultyStatus == 0 && result[i].inFacultyStatus == 0){
                    content += "<option value='" + result[i].clazzId + "'>" + result[i].name + "</option>"
                }
            }
            $("#addClassList").html(content);
            $('#addClassList').multiselect({
                enableFiltering: true,
                disableIfEmpty: true,
            });
            $( document ).ajaxStop(function() {
                if($('#addClassList').has('option').length < 0 ) {
                    $(".text-danger").show();
                }
                else if($('#addClassList').has('option').length > 0){
                    $(".text-danger").hide();
                }
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $.ajax({
        type: 'GET',
        accepts: 'application/json',
        contentType: 'application/json',
        url: API_SubjectsIndex,
        headers: {
            "Authorization": Cookies.get("token"),
        },
        success: function (result) {
            var content = "";
            for (var i in result){
                    content += "<option value='" + result[i].subjectId + "'>" + result[i].name + "</option>"
            }
            $("#addSubjectList").html(content);
            $('#addSubjectList').multiselect({
                enableFiltering: true,
                disableIfEmpty: true,
            });
        },
        error: function (xhr, textStatus, errorThrown) {
            alert("error");
        }
    });

    $(".btn-submit-date").click(function () {
        var formData = {
            "CourseId" : window.location.href.split("=")[1],
            "SubjectIds" : $("#addExamDate").val(),
            "ExamDate" : $("#chooseDate").val(),
        };
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_FacultySubjectExamDate,
            headers: {
                "Authorization": Cookies.get("token"),
            },
            data: JSON.stringify(formData),
            success: function (result) {
                alert("Added Exam Date Successful");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
    });

    $(".btn-submit-class").click(function (){
        var formData = {
            "FacultyId" : window.location.href.split("=")[1],
            "ClazzIds" : $("#addClassList").val()
        };
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_AddClazz,
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole")
            },
            data: JSON.stringify(formData),
            success: function (result) {
                alert("Added Classes Successful");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
    });

    $(".btn-submit-subject").click(function (){
        var formData = {
            "FacultyId" : window.location.href.split("=")[1],
            "SubjectIds" : $("#addSubjectList").val()
        };
        $.ajax({
            type: 'POST',
            accepts: 'application/json',
            contentType: 'application/json',
            url: API_AddSubjectToFaculty,
            headers: {
                "Authorization": Cookies.get("token"),
                "Role": Cookies.get("loggedUserRole")
            },
            data: JSON.stringify(formData),
            success: function (result) {
                alert("Added Subjects Successful");
                window.location.reload();
            },
            error: function (xhr, textStatus, errorThrown) {
                alert("error");
            }
        });
        console.log(formData)
    });

    $(document).on('click', '.delete-class-from-faculty' ,function(){
        if(confirm("Delete this class from this faculty?")){
            var class_facultyId = [];
            class_facultyId.push($(this).attr("class").split(' ')[1]);
            class_facultyId.push(window.location.href.split("=")[1]);
            $.ajax({
                accepts: 'application/json',
                contentType: 'application/json',
                type: 'POST',
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                url: API_DeleteClassFromFaculty,
                data: JSON.stringify(class_facultyId),
                success: function (result) {
                    alert("Deleted class from faculty");
                    window.location.reload();
                },
                error: function (xhr, textStatus, errorThrown) {
                }
            });
        }
    });

    $(document).on('click', '.delete-subject-from-faculty' ,function(){
        if(confirm("Delete this subject from this faculty?")){
            var formData = {
                "FacultyId" : window.location.href.split("=")[1],
                "SubjectId" : $(this).attr("class").split(' ')[1]
            };
            $.ajax({
                accepts: 'application/json',
                contentType: 'application/json',
                type: 'POST',
                headers: {
                    "Authorization": Cookies.get("token"),
                    "Role": Cookies.get("loggedUserRole"),
                },
                url: API_DeleteSubjectFromFaculty,
                data: JSON.stringify(formData),
                success: function (result) {
                    alert("Deleted subject from faculty");
                    window.location.reload();
                },
                error: function (xhr, textStatus, errorThrown) {
                }
            });
            console.log(subject_facultyId)
        }
    });
});